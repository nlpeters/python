''' 
Nancy Peters
9/2/2018
Module 11 Exercises
''' 

################### Exercise 1 - Prime Factors ###################
#Module 11 exercises (from .docx file)

#1.	Prime Factors. There are two kinds of positive numbers: prime numbers and composite numbers. 
#A composite number is the product of a sequence of prime numbers. You can write a simple function 
#to factor numbers and yield each prime factor of the number.


#Your factor function can accept a number, n, for factoring. The function will test values, f, 
#between 2 and the square root of n to see if the expression n % f == 0 is true. 
#If this is true. then the factor, f, divides n with no remainder; f is a factor.

import math			
def is_prime(number):
    if number > 1:
        if number == 2:
            return True
        if number % 2 == 0:
            return False
        for current in range(3, int(math.sqrt(number) + 1), 2):
            if number % current == 0: 
                return False
        return True
    return False
	
def prime_factor_generator(num):
    prime_list = []
    for i in range(2, int(math.sqrt(num) + 1)):
        if (num % i == 0) and is_prime(i):
            yield i

			
prime_factors = [is_prime(n) and for n in [x for x in range(100)]]

################### Exercise 2 - Fibonacci ###################
#2.	Create a generator to yield the numbers of a Fibonacci series.
#What is a Fibonacci number/series - https://en.wikipedia.org/wiki/Fibonacci_number

def fibonacci(num):
    if num <= 1:
       return num
    else:
       return fibonacci(num-1) + fibonacci(num-2)
	   
def fibonacci_generator():
    i = 0
    while True:
        i += 1
        yield fibonacci(i)
	   
fib_gen = fibonacci_generator()

next(fib_gen)
    

################### Problem 1 - Square Generator ###################	
# ------------------------------- 
#Module 11 exercises (from webpage via .html file)
#Problem 1
#Create a generator that generates the squares of numbers up to some number N.

def gensquares(N):
    for i in range(1, N+1):
       yield i**2
	
for x in gensquares(10):
    print(x)
	
################### Problem 2 - Random Generator ###################
#Problem 2
#Create a generator that yields "n" random numbers between 
#a low and high number (that are inputs).
# Note: Use the random library. For example:

#import random

#random.randint(1,10)
#6
import random
def rand_num(low,high,num):
    for i in range(num):
        yield random.randint(low, high)
		
for num in rand_num(1,10,12):
    print(num)
	
################### Problem 3 - iter function ###################
#Problem 3	
#Use the iter() function to convert the string below

s = 'hello'

#code here
s_iter_list = iter(list(s))

next(s_iter_list)


################### Problem 4 - Generator Explanations ###################
#Problem 4
#Explain a use case for a generator using a yield statement where you would not want to use a normal function with a return statement.
'''
Problem 4 Explanation:
As discussed in class, cases whereby statically storing the list would take up too much memory would be a common case where a 
generator would be preferred.  With a generator, the list would not be stored.  Instead, the next element in the list would be 
calculated at the time requested.
'''

#Extra Credit!
#Can you explain what gencomp is in the code below? 
#(Note: We never covered this in lecture! You will have to do some googling/Stack Overflowing!)

#my_list = [1,2,3,4,5]

#gencomp = (item for item in my_list if item > 3)

#for item in gencomp:
#    print item
#4
#5
#Hint google: generator comprehension is!
'''
Extra Credit Explanation:
As discussed in our class - a generator is also known as a tuple comprehension and, apparently, also known as a generator comprehension.
gencomp is a tuple comprehension/generator/generator comprehension.   As discussed, it releases items calculated via the code
within the generator, one by one (i.e. "lazily").  They are accessible either via a next function or within a for/in loop.  Once an item
has been iterated over the first time, it cannot be accessed again from that generator.  The only way to re-access that item
would be to redefine the generator and iterate over it again.
'''
''' 
Nancy Peters
9/18/2018
Module 8 Exercises
''' 

'''
Module 8 - Exercise 1
Write a program to prompt for a file name, and then read through the file line-by-line. Note: the file name is Erle.txtand its content is,

Erle is the enabling technology for the
next generation of aerial and terrestrial
robots that will be used in cities solving
tasks such as surveillance, enviromental
monitoring or even providing aid at catastrophes.

Ensure you create the file.
'''
def inputFileName():
    file_name = input('Please enter file name: ')
    return file_name

def printFile(file_obj):
    for line in file_obj:
        print(line)
	
def readPrintFile(file_name):
    f = open(file_name, 'r')
    printFile(f)
    f.close()
	
def module8FileExercise1():
    file_name = inputFileName()
    readPrintFile(file_name)
	
module8FileExercise1()
	
'''
Module 8 - Exercise 2
Create a file called new_world.txt.First add a new line to the file:Welcome to robotics time.. 
And then print the content of new_world.txt.
'''
def writeReadFile(file_name, text):
    f = open(file_name, 'w+')
    f.write(text)
    f.seek(0)
    f_text = f.read()
    print(f_text)
    f.close()
    
def module8FileExercise2():
    writeReadFile('new_world.txt', 'Welcome to robotics time..')
	
module8FileExercise2()

'''	
Module 8 - Exercise 3
Create a list 
L = [1,2,3,4,5]

Store the list in a pickle format in a file. 
Then retrieve the contents of the pickle file and store it back in a variable and print out the variable.
'''

def module8FileExercise3():
    import pickle
    pickle_file_name = "pickled_list.dat"
    l = [1,2,3,4,5]
    with open(pickle_file_name, "wb") as f:
        pickle.dump(l, f)
    with open(pickle_file_name, "rb") as f:
        read_l = pickle.load(f)
    print('The pickled and then unpickled list is: {}'.format(read_l))
	
module8FileExercise3()
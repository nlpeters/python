''' 
Nancy Peters
9/18/2018
Python batch 2 test
Programming Exercise 3 - Classical Fizz Buzz

•	Write a program that prints the numbers from 1 to 100 inclusive.
•	But for multiples of three print “Fizz” instead of the number
•	For the multiples of five print “Buzz”.
•	For numbers which are multiples of both three and five print “FizzBuzz” instead.

'''

def fizz(num):
    if (num % 3) == 0:
        return 'Fizz'
    else:
        return str(num)
	
def buzz(num):
    if (num % 5) == 0:
        return 'Buzz'
    else:
        return str(num)
	
def fizzBuzz(num):
    fizz_result = fizz(num)
    buzz_result = buzz(num)

    if fizz_result != 'Fizz': 
        fizz_result = ''
    elif buzz_result != 'Buzz':
        buzz_result = ''

    return fizz_result + buzz_result

# alternative for fun	
def fizzBuzzListRange(start_range, end_range_plus1):
    fizzbuzz_list = [fizzBuzz(n) for n in range(start_range, end_range_plus1)]

def fizzBuzzPrintRange(start_range, end_range_plus1):
    for n in range(start_range, end_range_plus1):
        print(fizzBuzz(n))
		
def fizzBuzzPrint1To100():
    fizzBuzzPrintRange(1,101)

# as list	
fizz_buzz_list_1_to_100 = fizzBuzzListRange(1,101)

# printed out
fizzBuzzPrint1To100()
''' 
Nancy Peters
9/9/2018
Module 14 Exercise
''' 

'''
Look at the code from the page https://docs.python.org/2/howto/descriptor.html 
and try to create a generic class which overrides __getattribute__() to perform the same task generically for all attributes

'''
class MyClass(object):

    def __getattribute__(self, name):	
        if super().__getattribute__(name):
            print ('Retrieving var "{}"'.format(name))
            value = object.__getattribute__(self, name)
            return value
        else:
            raise AttributeError			

    x = 10
    y = 5

m = MyClass()

m.x

m.y
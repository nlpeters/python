''' 
Nancy Peters
8/14/2018
Module 1.2 Exercises
''' 

# Module_1_2_Exercise_1: Words after G			

# Module_1_2_Exercise_1 function creation			
def words_after_g():	
	print('Module 1_2 Exercise 1 - Words after G:')
	input_sentence = input('Enter a 1 sentence quote, non-alpha separate words: ')	
	input_sentence_upper = input_sentence.upper() + '!'
	word = ''		
	for ch in input_sentence_upper:
		if ch.isalpha():
			word = word + ch		
		elif word > '':
			if word[0] >= 'H':
				print(word)
			word = ''

# Module_1_2_Exercise_1 function run
words_after_g()			

# Module_1_2_Exercise_2: Reverse a sentence with a twist

# Module_1_2_Exercise_1 function creation (5 functions)	

'''
Take given input string and run a given function on it that returns a string
Then print given output label, followed by the output string
'''				
def string_input_func_print(in_prompt, in_function, in_output_label):
	input_string = input(in_prompt)
	output_string = in_function(input_string)
	print(in_output_label)
	print(output_string)

# Reverse given sentence string and return result
def reverse_sentence(in_sentence):
    reverse_sentence = in_sentence[::-1]
    return reverse_sentence	

# Reverse each word in given sentence string, leaving words in order and return result	
def reverse_words_in_sentence(in_sentence):			
    in_sentence_words = in_sentence.split()
    reverse_word_sentence = ''
    for word in in_sentence_words:
        reverse_word_lower = word[::-1].lower()
        if reverse_word_sentence == '':
            reverse_word_sentence = reverse_word_lower
        else:
            reverse_word_sentence = reverse_word_sentence + ' ' + reverse_word_lower
    return reverse_word_sentence		

# Reverse each word in given sentence string, then reverse the order of the words and return result	
def reverse_sentence_by_words(in_sentence):			
    in_sentence_words = in_sentence.split()
    reverse_word_sentence_reverse = ''
    for word in in_sentence_words:
        reverse_word_lower = word[::-1].lower()
        reverse_word_sentence_reverse = reverse_word_lower + ' ' + reverse_word_sentence_reverse
    return reverse_word_sentence_reverse

# Get input and show output for all three Module_1_2 Exercise 2 exercises	
def rev_sentence_with_twist():
	
    print('Module 1_2 Exercise 2 - Reverse sentences with a twist:')

    # Reverse 1 - Reverse the statement.
    in_prompt = '2a. Enter a sentence to reverse: '
    in_function = reverse_sentence
    in_output_label = 'Reverse a sentence - part 1: '
    string_input_func_print(in_prompt, in_function, in_output_label)
	
	# Reverse 2 - Each word reversed.
    in_prompt = '2b. Enter sentence to have each word in order and each word reversed: '
    in_function = reverse_words_in_sentence
    in_output_label = 'Reverse a sentence - part 2: '
    string_input_func_print(in_prompt, in_function, in_output_label)	
	
	# Reverse 3 - The sentence reversed after each word is reversed.
    in_prompt = '2c. Enter sentence to have each word reversed, then sentence reversed: '
    in_function = reverse_sentence_by_words
    in_output_label = 'Reverse a sentence - part 3: '
    string_input_func_print(in_prompt, in_function, in_output_label)
	
# Module_1_2_Exercise_2 main function run
rev_sentence_with_twist()
	
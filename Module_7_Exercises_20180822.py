''' 
Nancy Peters
8/22/2018
Module 7 Exercises
''' 

################### Exercise 1 - Add exception handling to last_name function ###################
'''
EXERCISE GIVEN:
# Handle all the exceptions!
#Setup
actor = {"name": "John Cleese", "rank": "awesome"}

#Function to modify, should return the last name of the actor - think back to previous lessons for how to get it
def get_last_name():
    return actor["last_name"]

#Test code
get_last_name()
print("All exceptions caught! Good job!")
print("The actor's last name is %s" % get_last_name())
'''

#HOW CHANGED - replaced return statement with the try/except block of statements:
# Handle all the exceptions!
#Setup
actor = {"name": "John Cleese", "rank": "awesome"}

#Function to modify, should return the last name of the actor - think back to previous lessons for how to get it
def get_last_name():
    try:
        full_name = actor["name"]
        name_parts = full_name.split(' ')
        last_name = name_parts[1]
        return last_name
    except:
        return "Problem with name key in dictionary, which = %s" % full_name

#Test code
get_last_name()
print("All exceptions caught! Good job!")
print("The actor's last name is %s" % get_last_name())

################### Exercise 2 - Add exception handling to several functions ###################
'''
EXERCISE GIVEN:
The program below is not robust and prone to crashing on erroneous input data. 
Refactor the program to make it more robust with exception handling.

def example1():
    for i in range( 3 ):
        x = int( input( "enter a number: " ) )
        y = int( input( "enter another number: " ) )
        print( x, '/', y, '=', x/y )

def example2( L ):
    print( "\n\nExample 2" )
    sum = 0
    sumOfPairs = []
    for i in range( len( L ) ):
        sumOfPairs.append( L[i]+L[i+1] )

    print( "sumOfPairs = ", sumOfPairs )


def printUpperFile( fileName ):
   file = open( fileName, "r" )
   for line in file:
       print( line.upper() )
   file.close()
    
def main():
    example1()
    L = [ 10, 3, 5, 6, 9, 3 ]
    example2( L )
    example2( [ 10, 3, 5, 6, "NA", 3 ] )
    example3( [ 10, 3, 5, 6 ] )

    printUpperFile( "doesNotExistYest.txt" )
    printUpperFile( "./Dessssktop/misspelled.txt" )

main()

'''

#Added error handling, and corrected logic error within function example2
import logging

def example1():
    for i in range( 3 ):
        try:
            x = int( input( "enter a number: " ) )
            y = int( input( "enter another number: " ) )
            print( x, '/', y, '=', x/y )			
        except Exception as ex:
            logging.exception("Exception in function example1:  x={}, y={}, i={}".format(x,y,i))
		
def example2( L ):
    print( "\n\nExample 2" )
    sum = 0
    sumOfPairs = []
	#NOTE - corrected logic error below - used len(L)-1 instead of incorrect len(L)
    for i in range( len( L )-1 ):
        try:
            sumOfPairs.append( L[i]+L[i+1] )
        except Exception as ex:
            logging.exception("Exception in function example2:  sum={}, sumofPairs={}, L={}, i={}, i+1={}".format(sum,sumOfPairs,L,i,i+1))
				
    print( "sumOfPairs = ", sumOfPairs )

def printUpperFile( fileName ):
    try:
        file = open( fileName, "r" )
        for line in file:
            print( line.upper() )
        file.close()
    except Exception as ex:
        logging.exception("Exception in function printUpperFile:  fileName={}, line={}".format(fileName,line))	
    
def main():
    try:
        example1()
        L = [ 10, 3, 5, 6, 9, 3 ]
        example2( L )
        example2( [ 10, 3, 5, 6, "NA", 3 ] )
        example3( [ 10, 3, 5, 6 ] )

        printUpperFile( "doesNotExistYest.txt" )
        printUpperFile( "./Dessssktop/misspelled.txt" )
    except Exception as ex:	
        logging.exception("Exception in function main")
		
main()



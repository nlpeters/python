''' 
Nancy Peters
8/21/2018
Module 5 Exercises - Exercise 2
''' 

'''
•	Write a program which can filter even numbers in a list by using filter function. The list is: [1,2,3,4,5,6,7,8,9,10].
•	Write a program which can map() to make a list whose elements are square of elements in [1,2,3,4,5,6,7,8,9,10].
•	Combination of above two programs:
•		Write a program which can map() and filter() to make a list whose elements are square of even number in [1,2,3,4,5,6,7,8,9,10].
'''
def even_filtered_list(num_list):
    return list(filter(lambda n : (n%2)==0, num_list))

def squared_list(num_list):
    return list(map(lambda n : n*n, num_list))

def squared_even_filtered_list(num_list):
    even_filtered_num_list = even_filtered_list(num_list)
    return squared_list(even_filtered_num_list)

def module5_exercise2_test():
    num_list = [1,2,3,4,5,6,7,8,9,10]	
    print("Original num list = {}".format(num_list))	
    print("Even filtered list = {}".format(even_filtered_list(num_list)))
    print("Squared list = {}".format(squared_list(num_list)))
    print("Squared even filtered list = {}".format(squared_even_filtered_list(num_list)))
	
module5_exercise2_test()


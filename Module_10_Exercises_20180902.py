''' 
Nancy Peters
9/2/2018
Module 10 Exercises
''' 

################### Exercise 1 - List Comprehension Even Numbers ###################
#Let’s say I give you a list saved in a variable: a = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]. 
#Write one line of Python that takes this list a and makes a new list that has only the even elements of this list in it.

a = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]

even_a = [even_item for even_item in a if even_item%2 == 0]

################### Exercise 2a - Consonants ###################
#	A list of all consonants in the sentence 'The quick brown fox jumped over the lazy dog'

sentence = 'The quick brown fox jumped over the lazy dog'
consonants = [letter for letter in sentence if letter.lower() not in ['a', 'e', 'i', 'o', 'u', ' ']]
consonants_with_y_vowel = [letter for letter in sentence if letter.lower() not in ['a', 'e', 'i', 'o', 'u', 'y', ' ']]

################### Exercise 2b - Capitals ###################
#	A list of all the capital letters (and not white space) in 'The Quick Brown Fox Jumped Over The Lazy Dog'

sentence = 'The Quick Brown Fox Jumped Over The Lazy Dog'
caps = [letter for letter in sentence if letter>' ' and letter<'a']

################### Exercise 2c - Squares ###################
#	A list of all square numbers formed by squaring the numbers from 1 to 1000.

squares_of_1_to_1000 = [num**2 for num in range(1,1001)]

################### Exercise 3a - Divisible by 7 ###################
#	Find all of the numbers from 1-1000 that are divisible by 7

div_by_7_for_1_to_1000 = [num for num in range(1,1001) if num%7 == 0]

################### Exercise 3b - Has Digit 3 ###################
#	Find all of the numbers from 1-1000 that have a 3 in them

has_3_for_1_to_1000 = [num for num in range(1,1001) if str(num).find('3') >= 0]

################### Exercise 3c - Number of Spaces ###################
#	Count the number of spaces in a string
a_string = 'The quick brown fox jumped over the lazy dog'
spaces_in_string = [ch for ch in a_string if ch == ' ']
num_spaces_in_string = len(spaces_in_string)

################### Exercise 3d - Remove Vowels ###################
#	Remove all of the vowels in a string
a_string = 'The quick brown fox jumped over the lazy dog'
non_vowels = [ch for ch in a_string if ch.lower() not in ['a', 'e', 'i', 'o', 'u']]
non_vowel_string = ''.join(non_vowels)


################### Exercise 3e - Less-Than-Four-Letter Words ###################
#	Find all of the words in a string that are less than 4 letters

sentence = 'The quick brown fox jumped over the lazy dog'
words = sentence.split()
words_lt_4_letters = [word for word in words if len(word) < 4]


################### Challenge 1 - Word Length Dictionary ###################
#	Use a dictionary comprehension to count the length of each word in a sentence.

sentence = 'The quick brown fox jumped over the lazy dog'
words = sentence.split()
word_count_dict = {word:len(word) for word in words}

################### Challenge 2 - Divisible By Single Digits ###################
#	Use a nested list comprehension to find all of the numbers from 1-1000 that 
#	are divisible by any single digit besides 1 (2-9)
div_by_single_digit_for_1_to_1000 = [[num for num in range(1,1001) if num%div_by == 0] for div_by in range(2,10)]

################### Challenge 3 - Largest Single Digit Divisible By ###################
#	For all the numbers 1-1000, use a nested list/dictionary comprehension to find the highest single digit 
#	any of the numbers is divisible by
largest_single_digit_divisor_dict = {str(num) : max([div_by for div_by in range(2,10) if num%div_by == 0], default=-1) for num in range(1,1001)}

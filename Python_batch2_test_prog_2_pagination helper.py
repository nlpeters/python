''' 
Nancy Peters
9/18/2018
Python batch 2 test
Programming Exercise 2 - Pagination Helper (aka Page Fu Master)

The class is designed to take in an arrayof values and an integer indicating how many items will be allowed per each page. The types of values contained within the collection/array are not relevant.
The pagination_helper.py file provided in the folder is the file which needs to be completed. The following are some examples of how this class is used:
helper = PaginationHelper(['a','b','c','d','e','f'], 4) helper.page_count # should == 2 helper.item_count # should == 6 helper.page_item_count(0) # should == 4
helper.page_item_count(1) # last page - should == 2 helper.page_item_count(2) # should == -1 since the page is invalid

# page_ndex takes an item index and returns the page that it belongs on helper.page_index(5) # should == 1 (zero based index) helper.page_index(2) # should == 0
helper.page_index(20) # should == -1
helper.page_index(-10) # should == -1 because negative indexes are invalid

NOTE:  I added additional methods that were useful internally to the class
and seemed as if they might be useful externally as well.
'''

from math import floor

class PaginationHelper:

    # The constructor takes in an array of items and a integer indicating
    # how many items fit within a single page
    def __init__(self, collection, items_per_page):
        self.items = collection
        if items_per_page < 1:
            self.items_per_page = 1
            print('Items per page cannot be < 1, so set to default of 1.')
        else:
            self.items_per_page = items_per_page
            
    # returns the number of items within the entire collection
    def item_count(self):
        return len(self.items)
		
    # returns index of last item in collection
    def first_item_index(self):
        return 0	
		
    # returns index of last item in collection
    def last_item_index(self):
        return self.item_count() - 1		
		
    # returns the number of full pages (has items_per_page items number of items on page)
    def full_page_count(self):

        item_cnt = self.item_count()	
        full_page_cnt = math.floor(item_cnt / self.items_per_page)
        return full_page_cnt
		
    # returns the number of partial pages (less than items_per_page items on page)
    def partial_page_item_count(self):

        item_cnt = self.item_count()
        partial_page_item_cnt = item_cnt % self.items_per_page
        return partial_page_item_cnt

    # returns the number of partial pages (less than items_per_page items on page)
    def partial_page_count(self):

        partial_page_cnt = 0
        if self.partial_page_item_count() > 0:
            partial_page_cnt = 1
        return partial_page_cnt			
            
    # returns the number of pages
    def page_count(self):

        full_page_cnt = self.full_page_count()	
        partial_page_cnt = self.partial_page_count()
        page_cnt = full_page_cnt + partial_page_cnt
        return page_cnt

    # index of first page
    def first_page_index(self):
        return 0

    # index of last page
    def last_page_index(self):
        return self.page_count() - 1			
            	
    # returns the number of items on the current page. page_index is zero based
    # this method should return -1 for page_index values that are out of range
    def page_item_count(self, page_index):	

        page_item_cnt = self.items_per_page
        last_page_idx = self.last_page_index()
        if (page_index < self.first_page_index()) or (page_index > last_page_idx):
            page_item_cnt = -1
        elif (self.partial_page_count() == 1) and (page_index == last_page_idx):
            page_item_cnt = self.partial_page_item_count()
        return page_item_cnt
        			
    # determines what page an item is on. Zero based indexes.
    # this method should return -1 for item_index values that are out of range
    def page_index(self, item_index):
        if (item_index < self.first_item_index()) or (item_index > self.last_item_index()):
            page_idx = -1
        else:
            page_idx = math.floor(item_index / self.items_per_page)
        return page_idx
 
helper = PaginationHelper(['a','b','c','d','e','f'],4)

helper.item_count()

helper.page_count()

helper.page_item_count(0)

helper.page_item_count(1)

helper.page_item_count(2)

helper.page_item_count(-2)

helper.page_index(5)

helper.page_index(2)

helper.page_index(20)

helper.page_index(-10)

helper.first_item_index()

helper.last_item_index()

helper.full_page_count()

helper. partial_page_item_count()

helper.partial_page_count()

helper.first_page_index()

helper.last_page_index()

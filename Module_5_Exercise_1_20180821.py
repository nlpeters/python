''' 
Nancy Peters
8/21/2018
Module 5 Exercises - Exercise 1
''' 

################### Exercise 1-1 - Sphere Volume ###################

import math

def sphere_volume(r):
    v = (4/3) * math.pi * (r**3)
    return v

def test_sphere_volume():
    print("*************************************************")
    print("Module 5 - Exercise 1-1 - Sphere Volume")
    print("Sphere volume of 3 = {}".format(sphere_volume(3)))
    print("*************************************************")
	
test_sphere_volume()
	
################### Exercise 1-2 - Range Check and boolean version ###################
	
def range_check(n, a, b):
    if n in range(a, b+1):
        print( "{} is in inclusive range {}-{}".format(n,a,b))
    else :
        print( "{} is outside inclusive range {}-{}".format(n,a,b))

def test_range_check():		
    range_check(1,2,5)
    range_check(2,2,5)
    range_check(3,2,5)
    range_check(5,2,5)
    range_check(7,2,5)
	
def range_check_bool(n,a,b):
    in_range = n in range(a, b+1)
    return in_range

def test_range_check_bool():	
    print("1 in range is {}".format(range_check_bool(1,2,5)))
    print("2 in range is {}".format(range_check_bool(2,2,5)))
    print("3 in range is {}".format(range_check_bool(3,2,5)))
    print("5 in range is {}".format(range_check_bool(5,2,5)))
    print("7 in range is {}".format(range_check_bool(7,2,5)))
	
def test_range_functions():
    print("*************************************************")
    print("Module 5 - Exercise 1-2 - Range Check and boolean version")
    test_range_check()
    test_range_check_bool()	
    print("*************************************************")
	
test_range_functions()

################### Exercise 1-3 - Count upper-case and lower-case letters ###################

def in_char_range(in_ch, ch_low, ch_high):
    return ord(in_ch) in range(ord(ch_low), ord(ch_high)+1)

def up_low(in_str):
	num_up_chs = 0
	num_low_chs = 0
	for ch in in_str:
		if in_char_range(ch, 'A', 'Z'):
			num_up_chs += 1
		elif in_char_range(ch, 'a', 'z'):
			num_low_chs += 1
	print("No. of upper case characters: {}".format(num_up_chs))
	print("No. of lower case characters: {}".format(num_low_chs))
	
def test_up_low():
    print("*************************************************")
    print("Module 5 - Exercise 1-3 - Count upper-case and lower-case letters")	
    test_string = 'abNcEEEdz1AAxAZyZ*#'
    print("Function up_low test string: {}".format(test_string))
    up_low(test_string)
    print("*************************************************")	
	
test_up_low()

################### Exercise 1-4 - Unique List ###################

def unique_list(in_list):
    s = set(in_list)
    return list(s)

def test_unique_list():	
    print("*************************************************")
    print("Module 5 - Exercise 1-4 - Unique List")	
    test_list = [1,1,1,1,2,2,3,3,3,3,4,5]
    print("Original list {} yields unique list {}".format(test_list, unique_list(test_list)))
    print("*************************************************")
	
test_unique_list()

################### Exercise 1-5 - Multiply List ###################

def multiply_list(numbers):
    if len(numbers) == 0:
        return "Exception: Empty list"
    product = 1
    for num in numbers:
        product *= num
    return product

def test_multiply_list():
    print("*************************************************")
    print("Exercise 1-5 - Multiply List")	
    test_list = [1, 2, 3, -4]		
    print("List {} when elements are multiplied together equals {}".format(test_list, multiply_list(test_list)))
    print("*************************************************")
	
test_multiply_list()

################### Exercise 1-6 - Palindrome ###################

def is_palindrome(in_str):

	str_len = len(in_str)
	half_str_len = int(str_len / 2)	
	
	first_half_str_start = 0
	first_half_str_end = half_str_len
	first_half_str = in_str[first_half_str_start:first_half_str_end]
	
	sec_half_str_end = str_len
	sec_half_str_start = sec_half_str_end - half_str_len	
	sec_half_str = in_str[sec_half_str_start:sec_half_str_end]	
	sec_half_str_reversed = sec_half_str[::-1]
	
	is_palindrome_result = (first_half_str == sec_half_str_reversed)
	return is_palindrome_result
	
def test_is_palindrome():

    print("*************************************************")
    print("Module 5 - Exercise 1-6 - Palindrome")	

    palindrome_try1 = "abba"
    try1_result = is_palindrome(palindrome_try1)
    print("It is {} that '{}' is a palindrome.".format(try1_result, palindrome_try1))
	
    palindrome_try2 = "abcba"
    try2_result = is_palindrome(palindrome_try2)
    print("It is {} that '{}' is a palindrome.".format(try2_result, palindrome_try2))

    palindrome_try3 = "abbca"
    try3_result = is_palindrome(palindrome_try3)	
    print("It is {} that '{}' is a palindrome.".format(try3_result, palindrome_try3))
    print("*************************************************")	
	
test_is_palindrome()

################### Exercise 1-7 - Pangram ###################

import string

def dedupe_string(in_str):
    s = set(in_str)
    return "".join(s)

def sort_string(in_str):
    return "".join(sorted(in_str))

def alpha_check_str(in_str):
    lower_in_str = in_str.lower()
    deduped_lower_in_str = dedupe_string(lower_in_str)
    alpha_check_str = sort_string(deduped_lower_in_str).strip()
    return alpha_check_str

def is_pangram(in_str, alphabet=string.ascii_lowercase):  
    return (alpha_check_str(in_str) == alphabet)
	
def test_is_pangram():

    print("*************************************************")	    
    print("Module 5 - Exercise 1-7 - Pangram")	

    pangram_try1 = "The lazy brown fox jumped over the fence"
    try1_result = is_pangram(pangram_try1)
    print("It is {} that string '{}' is a pangram.".format(try1_result, pangram_try1))
    
    pangram_try2 = "The lazy brown fox jumped quickly over the fence"
    try2_result = is_pangram(pangram_try2)
    print("It is {} that string '{}' is a pangram.".format(try2_result, pangram_try2))
    pangram_try3 = "The quick brown fox jumps over the lazy dog"
    try3_result = is_pangram(pangram_try3)
    print("It is {} that string '{}' is a pangram.".format(try3_result, pangram_try3))
    print("*************************************************")		

test_is_pangram()

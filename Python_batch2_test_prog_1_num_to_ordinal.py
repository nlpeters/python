''' 
Nancy Peters
9/18/2018
Python batch 2 test
Programming Exercise 1 - Number to Ordinal

Finish the function numberToOrdinal – this is provided in the number_to_ordinal.py file,
which should take a number and return it as a string with the correct ordinal indicator 
suffix (in English). For example, 1 turns into "1st".
For the purposes of this challenge, you may assume that the function will always be 
passed a non- negative integer. If the function is given 0 as an argument, it should 
return '0' (as a string).
'''

def numberToOrdinal(number):

    num_str = str(number)
    second_to_last_digit = num_str[-2:-1]
    last_digit = num_str[-1]
	
    ordinal_suffix = 'th'

    if num_str == '0':
        ordinal_suffix = ''
    elif second_to_last_digit == '1':
        pass		
    elif last_digit == '1':
        ordinal_suffix = 'st'	
    elif last_digit == '2':	
        ordinal_suffix = 'nd'
    elif last_digit == '3':
        ordinal_suffix = 'rd'

    return num_str + ordinal_suffix

numberToOrdinal(0)	
	
numberToOrdinal(1)

numberToOrdinal(2)

numberToOrdinal(3)

numberToOrdinal(11)

numberToOrdinal(12)

numberToOrdinal(13)

numberToOrdinal(14)

numberToOrdinal(21)

numberToOrdinal(22)

numberToOrdinal(23)	

numberToOrdinal(24)	
		
numberToOrdinal(110)

numberToOrdinal(111)
		
numberToOrdinal(112)

numberToOrdinal(113)

numberToOrdinal(121)
		
numberToOrdinal(122)
		
numberToOrdinal(123)
		
numberToOrdinal(124)
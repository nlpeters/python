''' 
Nancy Peters
8/23/2018
Module 7 Exercise 2 Revised from 8/22 version
''' 

'''
EXERCISE GIVEN:
The program below is not robust and prone to crashing on erroneous input data. 
Refactor the program to make it more robust with exception handling.

def example1():
    for i in range( 3 ):
        x = int( input( "enter a number: " ) )
        y = int( input( "enter another number: " ) )
        print( x, '/', y, '=', x/y )

def example2( L ):
    print( "\n\nExample 2" )
    sum = 0
    sumOfPairs = []
    for i in range( len( L ) ):
        sumOfPairs.append( L[i]+L[i+1] )

    print( "sumOfPairs = ", sumOfPairs )


def printUpperFile( fileName ):
   file = open( fileName, "r" )
   for line in file:
       print( line.upper() )
   file.close()
    
def main():
    example1()
    L = [ 10, 3, 5, 6, 9, 3 ]
    example2( L )
    example2( [ 10, 3, 5, 6, "NA", 3 ] )
    example3( [ 10, 3, 5, 6 ] )

    printUpperFile( "doesNotExistYest.txt" )
    printUpperFile( "./Dessssktop/misspelled.txt" )

main()

'''

#Added error handling, and, unlike other version, did not correct logic error within function example2
import sys

def example1():
    for i in range( 3 ):
        try:
            x = int( input( "enter a number: " ) )
            y = int( input( "enter another number: " ) )
            print( x, '/', y, '=', x/y )
        except ValueError as err:
            print("Exception NON-NUMBER ENTERED in function example1:  x={}, y={}, i={}".format(x,y,i))		
            print(err)
        except ZeroDivisionError as err:
            print("Exception ZERO DIVISION in function example1:  x={}, y={}, i={}".format(x,y,i))		
            print(err)
        except:
            print("Exception in function example1:  x={}, y={}, i={}".format(x,y,i))
            err = sys.exc_info()[1]
            print(err)
		
def example2( L ):
    print( "\n\nExample 2" )
    sum = 0
    sumOfPairs = []
	#NOTE - originally corrected logic error below - used len(L)-1 instead of incorrect len(L) - changed back
    for i in range( len( L ) ):
        try:
            sumOfPairs.append( L[i]+L[i+1] )
        except IndexError as err:
            print("Exception INDEX in function example2:  sum={}, sumofPairs={}, L={}, i={}, i+1={}".format(sum,sumOfPairs,L,i,i+1))	
            print(err)
        except:
            print("Exception in function example2:  sum={}, sumofPairs={}, L={}, i={}, i+1={}".format(sum,sumOfPairs,L,i,i+1))
            err = sys.exc_info()[1]
            print(err)			

    print( "sumOfPairs = ", sumOfPairs )

def printUpperFile( fileName ):
    try:
        file = open( fileName, "r" )
        for line in file:
            print( line.upper() )
        file.close()
    except FileNotFoundError as err:
        print("Exception FILE NOT FOUND in function printUpperFile:  fileName={}, line={}".format(fileName,line))		
        print(err)
    except:
        print("Exception in function printUpperFile:  fileName={}, line={}".format(fileName,line))
        err = sys.exc_info()[1]
        print(err)				
    
def main():
    try:
        example1()
        L = [ 10, 3, 5, 6, 9, 3 ]
        example2( L )
        example2( [ 10, 3, 5, 6, "NA", 3 ] )
        example3( [ 10, 3, 5, 6 ] )

        printUpperFile( "doesNotExistYest.txt" )
        printUpperFile( "./Dessssktop/misspelled.txt" )
    except:
        print("Exception in function main")
        err = sys.exc_info()[1]
        print(err)	
		
main()



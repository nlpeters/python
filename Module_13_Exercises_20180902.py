''' 
Nancy Peters
9/2/2018
Module 13 Exercises
''' 

################### Exercise 1 - Timing Context Manager ###################
'''
1.	Timing Context Manager
Create a context manager that will print to stdout the 
elapsed time taken to run all the code inside the context:
'''
#Version 1: context manager 
class Timer:
    import datetime
    def __enter__(self): 
        self.start_time = datetime.datetime.now()
        self.end_time = datetime.datetime.now()

    def __exit__(self, *args):
        self.end_time = datetime.datetime.now()
        time_elapsed = self.end_time - self.start_time
        secs_elapsed = time_elapsed.total_seconds()
        print('this code took {} seconds'.format(secs_elapsed))	
        
# Version 1 Timer context manager test
with Timer() as t:
    for i in range(100000):
        i = i ** 20
		

#Version 2: context manager created with contextlib contextmanager decorator	
from contextlib import contextmanager
import datetime
	
@contextmanager
def Timer():
    start_time = datetime.datetime.now()
    end_time = datetime.datetime.now()
    yield None
    end_time = datetime.datetime.now()
    time_elapsed = end_time - start_time
    secs_elapsed = time_elapsed.total_seconds()
    print('this code took {} seconds'.format(secs_elapsed))	
        
# Version 2 Timer context manager test
with Timer() as t:
    for i in range(100000):
        i = i ** 20 

################### Exercise 2 - File Context Manager ###################
'''
2.	Create a replica of the open_file() method as a File context manager class 
#which safely closes the file before ending the context in which the file is used. 
#It may be used as 

with File('demo.txt', 'wb') as opened_file:
    opened_file.write('Hola!')

'''

'''
2.	File Context Manager
Create a context manager that will print to stdout the 
elapsed time taken to run all the code inside the context:
'''
#Version 1: context manager 
class File:
    def __init__(self, filename, mode):
        self.filename = filename
        self.mode = mode

    def __enter__(self): 
        self.open_file = open(self.filename, self.mode)
        return self.open_file

    def __exit__(self, *args):
        self.open_file.close()
        
# Version 1 File context manager test
with File('demo.txt', 'w') as opened_file:
    opened_file.write('Hola!')
		

#Version 2: context manager created with contextlib contextmanager decorator	
from contextlib import contextmanager
	
@contextmanager
def File(filename, mode):
    open_file = open(filename, mode)
    yield open_file
    open_file.close()
	
# Version 2 File context manager test
with File('demo.txt', 'w') as opened_file:
    opened_file.write('Hola!')

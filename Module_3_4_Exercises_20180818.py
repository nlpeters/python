''' 
Nancy Peters
8/18/2018
Module 3.4 Exercises
''' 

################### Exercise 1 - Divisors ###################
def print_divisor_list():

	print("*************************************************")
	print('Module 3_4 Exercise 1 - Divisors:')
	
	in_int = int(input ("Please enter integer to get list of divisors: "))
	in_int_half_plus1 = int(in_int / 2) + 1
	divisor_list = list()
	for i in range(1, in_int_half_plus1):
		if ((in_int % i) == 0):
			divisor_list.append(i)
	divisor_list.append(in_int)
	
	print('Divisors of', in_int, 'are:')
	print(divisor_list)
	
	print("*************************************************")
	
print_divisor_list()

################### Exercise 2 - Palindrome ###################	
'''
Take given input string and run a given function on it that returns a string
Then print given output label, followed by the output string
'''				
def string_input_func_print(in_prompt, in_function, in_output_label):
	input_string = input(in_prompt)
	output_string = in_function(input_string)
	print(in_output_label)
	print(output_string)		
				
def is_palindrome(in_str):

	str_len = len(in_str)
	half_str_len = int(str_len / 2)	
	
	first_half_str_start = 0
	first_half_str_end = half_str_len
	first_half_str = in_str[first_half_str_start:first_half_str_end]
	
	sec_half_str_end = str_len
	sec_half_str_start = sec_half_str_end - half_str_len	
	sec_half_str = in_str[sec_half_str_start:sec_half_str_end]	
	sec_half_str_reversed = sec_half_str[::-1]
	
	is_palindrome_result = (first_half_str == sec_half_str_reversed)
	return is_palindrome_result
	
def is_palindrome_str_reply(in_str):
	if(is_palindrome(in_str)):
		return "'" + in_str + "' is a palindrome!"
	else:
		return "'" + in_str + "' is a not palindrome!"		
	
def palindrome():

    print("*************************************************")
    print('Module 3_4 Exercise 2 - Palindrome:')

    in_prompt = '2. Enter word to identify as palindrome: '
    in_function = is_palindrome_str_reply
    in_output_label = 'Is entered word palindrome?'
    string_input_func_print(in_prompt, in_function, in_output_label)
    
    print("*************************************************")
	
palindrome()
	
################### Exercise 3 - Character Input including Bonus ###################		
def input_name():
    input_name = input ("Please enter your name: ")	
    return input_name
	
def input_age():
    input_age = int(input ("Please enter your current age: "))
    return input_age
	
def when_age(in_name, in_current_age, in_age_to_become):

    from datetime import datetime
    years_til_age_to_become = in_age_to_become - in_current_age
    today = datetime.today()
    current_year = today.year
    year_turn_age_to_become2 = current_year + years_til_age_to_become
    year_turn_age_to_become1 = year_turn_age_to_become2 - 1
    response_sentence = "{}, you will be age {} in the year {} or {}."
    response = response_sentence.format(in_name, in_age_to_become, year_turn_age_to_become1, year_turn_age_to_become2)
    return response	
	
def when_age_100(in_name, in_current_age):
    return when_age(in_name, in_current_age, 100)	
	
def user_when_age_100():

    user_name = input_name()
    user_age = input_age()
    user_when_age_100_response = when_age_100(user_name, user_age)
    return user_when_age_100_response
	
def str_ntimes_lines(in_str, n):
	return (in_str + "\nin_name, in_current_age, ") * n
	
def print_str_ntimes_lines(in_str, n):
	print(str_ntimes_newlines(in_str, n))
	
def user_when_age_100_ntimes():
    print("*************************************************")
    print('Module 3_4 Exercise 3 - Character Input including Bonus:')

    n = int(input("How many times should answer be repeated? "))
    user_when_age_100_sentence = user_when_age_100()
    print_str_ntimes_lines(user_when_age_100_sentence, n)
    
    print("*************************************************")
    
	
user_when_age_100_ntimes()

	



	
	
def copy_function_source(func, new_func_name):
    import inspect
    func_full_source = inspect.getsource(func)
    func_lines_by_colon = func_full_source.split(':')
    func_def_line = func_lines_by_colon[0]
    func_source_no_signature = func_full_source.replace(func_def_line, '')	
    func_signature = func_def_line.replace('def ', '').strip()
    func_signature_by_open_paren = func_signature.split('(')
    func_name = func_signature_by_open_paren[0].strip()
    func_rest_of_signature = func_signature_by_open_paren[1]
    func_new_def_line = 'def ' + new_func_name + '(' + func_rest_of_signature	
    func_w_new_name_full_source = func_new_def_line + func_source_no_signature
    return func_w_new_name_full_source
	      
def copy_function(func, new_func_name):
    func_w_new_name_source = copy_function_source(func, new_func_name)
    print('New function source code is: ')
    print(func_w_new_name_source)
    executable_source = '""\n' + func_w_new_name_source + '""'
    exec(executable_source, globals())
''' 
Nancy Peters
9/19/2018
Module 12 Exercises
''' 

'''
1.	Design a decorator to write a function return value in a text file. The decorator may be used as below.
 
@log_message
def a_function_that_returns_a_string():
    return "a string"

@log_message
def a_function_that_returns_a_strings_with_a_newline(s):
    return "{}\n".format(s)
 
Hint : https://stackoverflow.com/questions/47470645/design-a-decorator-to-write-a-function-return-value-in-a-text-file
''' 

def log_message(func):

    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        f = open('log.txt', 'a')
        f.write(result)		
        f.close()
        return result

    return wrapper

@log_message
def a_function_that_returns_a_string():
    return "a string"

@log_message
def a_function_that_returns_a_strings_with_a_newline(s):
    return "{}\n".format(s)


a = a_function_that_returns_a_string()
b = a_function_that_returns_a_strings_with_a_newline('abc')



'''
2.	Write a simple decorator you can apply to a function that returns a string. 
Decorating such a function should result in the original output, wrapped by an HTML ‘p’ tag.
''' 

def html_p(func):

    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        return '<p>' + result + '</p>'

    return wrapper

@html_p
def returns_given_string(str):
    return str

returns_given_string('test decorator')

''' 
Nancy Peters
9/7/2018
Module 9 Exercises
''' 

################### Exercise from 9/5 Class - Years in Mandela File ###################
'''
Regex Exercise
1. Extract all the text in the wikipedia page for Nelson Mandela

2. Save to a file.

3. Write a program to read the file

4. Write a regular expression to extract all dates after 1500AD 
'''

import re
import string

def years_after_1500ad_list(str):
    years = re.findall(r'\d{4,4}', str)
    years_after_1500ad = [n for n in years if int(n) > 1500]
    return years_after_1500ad
	
def read_file_to_string(file_name):
    file = open(file_name, "r")
    lines_list = file.readlines()
    lines = ''.join(lines_list)
    file.close
    return lines
	
def years_after_1500ad_in_file(file_name):
    file_string = read_file_to_string(file_name)
    #print('Here is content of given file {}: {}'.format(file_name, file_string)) 	
    years_after_1500ad = years_after_1500ad_list(file_string)
    print('Here is a list of all the years after 1500AD in given file {}: {}'.format(file_name, years_after_1500ad)) 
	
years_after_1500ad_in_file('mandela2.txt')

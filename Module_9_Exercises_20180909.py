''' 
Nancy Peters
9/9/2018
Module 9 Exercises
''' 

"""
Regex Validation Exercises

These functions return ``True`` or ``False`` depending on whether the
string passes a condition.

"""

def has_vowel(string):
    """Return True iff the string contains one or more vowels."""
    return bool(re.search(r'.*[aeiou].*', string))

def is_integer(string):
    """Return True iff the string represents a valid integer. (optional minus 1+ digits)"""
    return bool(re.search(r'^-{0,1}\d+', string))

def is_fraction(string):
    """Return True iff the string represents a valid fraction. (optional minus 1+ digits slash 1+ digits and no 0 denominator)"""
    return bool(re.search(r'^-{0,1}\d+/\d*[1-9]\d*$', string))

def is_valid_date(string):
    """Return True iff the string represents a valid YYYY-MM-DD date."""
    return bool(re.search(r'^\d{4}-((0[1-9])|(1[0-2]))-((0[1-9])|([1-2][0-9])|(3[0-1]))$', string))

def is_number(string):
    """Return True iff the string represents a decimal number. (optional minus 1+ digits with optional period and final 1+ digits)"""
    return bool(re.search(r'^((-{0,1}\d*\.\d+)|(-{0,1}\d+))$', string))	

def is_hex_color(string):
    """Return True iff the string represents an RGB hex color code. (hash exactly 3 hex digits or hash  exactly 6 hex digits)"""
    return bool(re.search(r'^((#[\da-f]{3})|(#[\da-f]{6}))$', string))
	
